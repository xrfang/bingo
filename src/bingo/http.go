package main

import (
	"bytes"
	"html/template"
	"net/http"
	"path"
	"strings"
	"time"
)

func modified(w http.ResponseWriter, r *http.Request) bool {
	ims, err := time.Parse(time.RFC1123, r.Header.Get("If-Modified-Since"))
	if err != nil || LastModified.UnixNano() > ims.UnixNano() {
		return true
	}
	http.Error(w, http.StatusText(304), 304)
	return false
}

func sendAsset(w http.ResponseWriter, name string) {
	if strings.HasPrefix(name, "/") {
		name = name[1:]
	}
	data, err := Asset(name)
	if err != nil {
		http.Error(w, "", http.StatusNotFound)
		return
	}
	switch strings.ToLower(path.Ext(name)) {
	case ".css":
		w.Header().Add("Content-Type", "text/css")
	case ".js":
		w.Header().Add("Content-Type", "application/javascript")
	case ".jpg", ".jpeg":
		w.Header().Add("Content-Type", "image/jpeg")
	case ".png":
		w.Header().Add("Content-Type", "image/png")
	default:
		w.Header().Add("Content-Type", "application/octet-stream")
	}
	w.Header().Add("Last-Modified", _BUILT_)
	w.Write(data)
}

func renderTemplate(w http.ResponseWriter, tpl string, args interface{}) {
	var buf bytes.Buffer
	defer func() {
		if e := recover(); e != nil {
			logger.Err(e.(error).Error())
			http.Error(w, e.(error).Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Add("Content-Type", "text/html; charset=utf-8")
		w.Write(buf.Bytes())
	}()
	helper := template.FuncMap{
		"ver": func() string {
			return "V" + _G_REVS + "." + _G_HASH
		},
	}
	body, err := Asset("templates/" + tpl)
	assert(err)
	t, err := template.New("body").Funcs(helper).Parse(string(body))
	assert(err)
	header, _ := Asset("templates/header.html")
	footer, _ := Asset("templates/footer.html")
	t, err = t.Parse(string(header))
	assert(err)
	t, err = t.Parse(string(footer))
	assert(err)
	assert(t.Execute(&buf, args))
}
