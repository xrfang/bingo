package main

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"github.com/pquerna/otp/totp"
)

type authInfo struct {
	Name   string `json:"name"`
	Secret string `json:"secret"`
}

type AuthDB struct {
	path string
	info map[string]authInfo
}

func (adb AuthDB) Validate(user, pass string) bool {
	ai, ok := adb.info[user]
	if !ok {
		return false
	}
	return totp.Validate(pass, ai.Secret)
}

func (adb *AuthDB) Load(path string) {
	adb.path = path
	adb.info = make(map[string]authInfo)
	f, err := os.Open(path)
	if err != nil {
		if os.IsNotExist(err) {
			return
		}
		panic(err)
	}
	defer f.Close()
	dec := json.NewDecoder(f)
	assert(dec.Decode(&adb.info))
}

func (adb *AuthDB) Set(user, secret string) (err error) {
	defer func() {
		if e := recover(); e != nil {
			err = e.(error)
		}
	}()
	if adb.path == "" {
		panic(fmt.Errorf("AUTHDB not set"))
	}
	ai := adb.info[user]
	ai.Name = user
	ai.Secret = secret
	adb.info[user] = ai
	os.Rename(adb.path, adb.path+time.Now().Format(".20060102150405"))
	f, err := os.Create(adb.path)
	assert(err)
	defer f.Close()
	enc := json.NewEncoder(f)
	enc.SetIndent("", "    ")
	assert(enc.Encode(adb.info))
	return
}

var authDb AuthDB
