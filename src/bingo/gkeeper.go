package main

import (
	"net"
	"net/http"
	"strings"
	"sync"
)

const MAX_SESSIONS = 100

type gateKeeper struct {
	cli map[string]string //map from IP to username
	sync.Mutex
}

func (gk *gateKeeper) Register(name string, r *http.Request) bool {
	gk.Lock()
	defer gk.Unlock()
	if len(gk.cli) >= MAX_SESSIONS {
		return false
	}
	for k, v := range gk.cli {
		if v == name {
			delete(gk.cli, k)
		}
	}
	ip := strings.SplitN(r.RemoteAddr, ":", 2)[0]
	gk.cli[ip] = name
	return true
}

func (gk *gateKeeper) Validate(r *http.Request) string {
	gk.Lock()
	defer gk.Unlock()
	ip := strings.SplitN(r.RemoteAddr, ":", 2)[0]
	return gk.cli[ip]
}

func (gk *gateKeeper) Lookup(src net.Addr) string {
	gk.Lock()
	defer gk.Unlock()
	ip := strings.SplitN(src.String(), ":", 2)[0]
	return gk.cli[ip]
}

func (gk gateKeeper) Revoke(r *http.Request) {
	ip := strings.SplitN(r.RemoteAddr, ":", 2)[0]
	gk.Unregister(ip)
}

func (gk gateKeeper) Unregister(ip string) {
	gk.Lock()
	defer gk.Unlock()
	delete(gk.cli, ip)
}

var guard gateKeeper

func init() {
	guard.cli = make(map[string]string)
}
