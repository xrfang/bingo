package main

import (
	"expvar"
	"net/http"
	"os"
	"strconv"
	"time"
)

var LastModified time.Time

func main() {
	loadConfig()

	expvar.NewString("epoch").Set(time.Now().Format(time.RFC3339))
	expvar.NewString("version").Set(_G_REVS + "." + _G_HASH)
	expvar.NewInt("pid").Set(int64(os.Getpid()))
	expvar.Publish("config", cf)

	bt, _ := strconv.Atoi(_BUILT_)
	LastModified = time.Unix(int64(bt), 0)
	mux := http.NewServeMux()
	mux.HandleFunc("/", home)
	mux.HandleFunc("/cmdline", cmdline)
	mux.HandleFunc("/debug/clients", clients)
	mux.HandleFunc("/debug/kill", kill)
	mux.HandleFunc("/debug/peek", peek)
	mux.HandleFunc("/debug/vars", expvarHandler)
	mux.HandleFunc("/explore", explore)
	mux.HandleFunc("/favicon.ico", favicon)
	mux.HandleFunc("/login", login)
	mux.HandleFunc("/logout", logout)
	mux.HandleFunc("/upload", upload)
	mux.HandleFunc("/view", view)
	mux.HandleFunc("/ws", ws)
	svr := http.Server{
		Addr:         ":" + cf.ADMIN_PORT,
		Handler:      mux,
		ReadTimeout:  time.Duration(cf.READ_TIMEOUT) * time.Second,
		WriteTimeout: time.Duration(cf.WRITE_TIMEOUT) * time.Second,
	}
	go svr.ListenAndServeTLS(cf.TLS_CERT, cf.TLS_PKEY)
	proxy()
}
