package main

import (
	"net/http"
)

func logout(w http.ResponseWriter, r *http.Request) {
	guard.Revoke(r)
	http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
}
