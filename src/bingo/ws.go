package main

import (
	"net/http"
	"os/exec"

	"github.com/kr/pty"

	"github.com/gorilla/websocket"
)

func ws(w http.ResponseWriter, r *http.Request) {
	defer func() {
		if e := recover(); e != nil {
			logger.Err(trace("ws: %v", e)...)
		}
	}()
	up := websocket.Upgrader{}
	conn, err := up.Upgrade(w, r, nil)
	assert(err)
	defer conn.Close()
	cmd := exec.Command(cf.SHELL)
	p, err := pty.Start(cmd)
	assert(err)
	defer func() {
		cmd.Process.Kill()
		p.Close()
	}()
	go cmd.Wait()
	go func() {
		buf := make([]byte, 1024)
		for {
			n, err := p.Read(buf)
			if err != nil {
				return
			}
			if conn.WriteMessage(1, buf[:n]) != nil {
				return
			}
		}
	}()
	for {
		_, msg, err := conn.ReadMessage()
		assert(err)
		_, err = p.Write(msg)
		assert(err)
	}
}
