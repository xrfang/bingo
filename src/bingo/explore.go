package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"os/user"
	"path/filepath"
	"sort"
	"strings"
)

func contentType(fn string) (ctype string) {
	ctype = "application/octet-stream"
	f, err := os.Open(fn)
	if err != nil {
		return
	}
	defer f.Close()
	head := make([]byte, 512)
	n, err := io.ReadFull(f, head)
	if err != nil && err != io.ErrUnexpectedEOF {
		return
	}
	return http.DetectContentType(head[:n])
}

func human(size int64) string {
	if size < 1024 {
		return fmt.Sprintf("%d", size)
	} else if size < 1024*1024 {
		return fmt.Sprintf("%0.2fK", float64(size)/float64(1024))
	} else if size < 1024*1024*1024 {
		return fmt.Sprintf("%0.2fM", float64(size)/float64(1024*1024))
	}
	return fmt.Sprintf("%0.2fG", float64(size)/float64(1024*1024*1024))
}

func ls(path string) (list interface{}, err error) {
	defer func() {
		if e := recover(); e != nil {
			list = nil
			err = e.(error)
		}
	}()
	ps := []string{"/"}
	for _, p := range strings.Split(path, "/") {
		if len(p) > 0 {
			ps = append(ps, p)
		}
	}
	path = "/" + strings.Join(ps[1:], "/")
	var (
		entries                []map[string]interface{}
		totalS, totalF, totalD int64
	)
	fs, err := filepath.Glob(filepath.Join(path, "*"))
	assert(err)
	sort.Slice(fs, func(i, j int) bool {
		si := strings.ToLower(fs[i])
		sj := strings.ToLower(fs[j])
		return si < sj
	})
	for _, f := range fs {
		st, err := os.Stat(f)
		if err != nil {
			continue
		}
		entry := map[string]interface{}{
			"name":     st.Name(),
			"modified": st.ModTime().Format("2006-01-02 15:04:05"),
		}
		if st.IsDir() {
			totalD++
			entry["type"] = "inode/directory"
			fs, _ := filepath.Glob(filepath.Join(f, "*"))
			entry["size"] = len(fs)
		} else if st.Mode().IsRegular() {
			entry["type"] = contentType(f)
			entry["size"] = human(st.Size())
			totalF++
			totalS += st.Size()
		} else {
			continue //not a regular file
		}
		entries = append(entries, entry)
	}
	list = map[string]interface{}{
		"total": map[string]interface{}{
			"dirs":  totalD,
			"files": totalF,
			"size":  human(totalS),
		},
		"path":    ps,
		"entries": entries,
	}
	return
}

func explore(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Query().Get("path")
	if path == "" {
		usr, err := user.Current()
		if err == nil {
			path = usr.HomeDir
		} else {
			path = "/"
		}
	}
	http.SetCookie(w, &http.Cookie{
		Name:   "pwd",
		Value:  path,
		Path:   "/",
		MaxAge: 0,
		Secure: true,
	})
	list, err := ls(path)
	enc := json.NewEncoder(w)
	if err != nil {
		assert(enc.Encode(map[string]interface{}{
			"stat": false,
			"data": err.Error(),
		}))
		return
	}
	assert(enc.Encode(map[string]interface{}{
		"stat": true,
		"data": list,
	}))
}
