package main

import (
	"strings"
	"sync"
	"time"
)

type Logger struct {
	ch chan []string
	sync.Mutex
}

func pad(msg []string) []string {
	ts := time.Now().Format(time.RFC3339) + " "
	padding := strings.Repeat(" ", len(ts))
	msg[0] = ts + msg[0]
	for i := 1; i < len(msg); i++ {
		msg[i] = padding + msg[i]
	}
	return msg
}

func (lg *Logger) Connect() chan []string {
	lg.Lock()
	defer lg.Unlock()
	if lg.ch != nil {
		close(lg.ch)
	}
	lg.ch = make(chan []string, 1024)
	return lg.ch
}

func (lg *Logger) Log(msg ...string) {
	if len(msg) == 0 {
		return
	}
	lg.Lock()
	defer lg.Unlock()
	if lg.ch == nil {
		return
	}
	msg = pad(msg)
	lg.ch <- msg
}

func (lg *Logger) Err(msg ...string) {
	if len(msg) == 0 {
		return
	}
	msg[0] = "[ERROR] " + msg[0]
	lg.Log(msg...)
}

var logger Logger
