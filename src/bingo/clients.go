package main

import (
	"encoding/json"
	"net/http"
)

func clients(w http.ResponseWriter, r *http.Request) {
	if guard.Validate(r) == "" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	enc := json.NewEncoder(w)
	guard.Lock()
	enc.Encode(guard.cli)
	guard.Unlock()
}
