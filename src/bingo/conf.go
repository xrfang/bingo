package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"path"
	"regexp"

	"github.com/mdp/qrterminal"
	"github.com/pquerna/otp"
	"github.com/pquerna/otp/totp"
	"github.com/xrfang/go-conf"
)

type Configuration struct {
	ADMIN_PORT    string
	AUTHDB        string
	BACKDOOR      string
	DOMAIN_NAME   string
	OTP_DIGITS    int
	OTP_ISSUER    string
	OTP_TIMEOUT   uint
	PROXY_PORT    string
	READ_TIMEOUT  int
	SHELL         string
	TLS_CERT      string
	TLS_PKEY      string
	WRITE_TIMEOUT int
	root          string
}

func (c Configuration) String() string {
	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.Encode(c)
	return buf.String()
}

func (c Configuration) canonicalize(fn string) string {
	if fn == "" {
		return ""
	}
	if !path.IsAbs(fn) {
		fn = path.Clean(path.Join(c.root, fn))
	}
	return fn
}

var cf Configuration

func validate(fn, key string) {
	st, err := os.Stat(fn)
	if err != nil || st.IsDir() {
		fmt.Printf("configuration error: missing or invalid %s\n", key)
		os.Exit(1)
	}
}

func loadConfig() {
	ver := flag.Bool("version", false, "show version info")
	cfg := flag.String("conf", "", "configuration file")
	usr := flag.String("user", "", "setup/modify user account")
	tpl := flag.Bool("init", false, "create a sample configuration file")
	flag.Parse()
	if *ver {
		fmt.Println(verinfo())
		os.Exit(0)
	}
	if *tpl {
		_, err := os.Stat("bingo.conf")
		if err == nil {
			fmt.Println("ERROR: ./bingo.conf already exists")
			os.Exit(1)
		}
		f, err := os.Create("bingo.conf")
		if err != nil {
			fmt.Printf("ERROR: cannot create ./bingo.conf (%v)\n", err)
			os.Exit(1)
		}
		defer f.Close()
		cfg, _ := Asset("init/bingo.conf")
		f.Write(cfg)
		os.Exit(0)
	}
	if *cfg == "" {
		fmt.Println("missing configuration file, try -help")
		os.Exit(1)
	}
	//default values
	cf.ADMIN_PORT = "2366"
	cf.AUTHDB = "bingo_auth.json"
	cf.PROXY_PORT = "7799"
	cf.OTP_DIGITS = 6
	cf.OTP_ISSUER = "bingo"
	cf.OTP_TIMEOUT = 30
	cf.READ_TIMEOUT = 300
	cf.WRITE_TIMEOUT = 300
	assert(conf.ParseFile(*cfg, &cf))
	cf.root = path.Dir(*cfg)
	if cf.DOMAIN_NAME == "" {
		fmt.Println("invalid configuration: missing DOMAIN_NAME")
		os.Exit(1)
	}
	if cf.SHELL == "" {
		cf.SHELL = ENV["SHELL"]
	}
	cf.AUTHDB = cf.canonicalize(cf.AUTHDB)
	cf.TLS_CERT = cf.canonicalize(cf.TLS_CERT)
	validate(cf.TLS_CERT, "TLS_CERT")
	cf.TLS_PKEY = cf.canonicalize(cf.TLS_PKEY)
	validate(cf.TLS_PKEY, "TLS_PKEY")
	authDb.Load(cf.AUTHDB)
	if *usr != "" {
		rx := regexp.MustCompile(`^\w{1,16}$`)
		if !rx.MatchString(*usr) {
			fmt.Println(`ERROR: username must match ^\w{1,16}$`)
			return
		}
		gopts := totp.GenerateOpts{
			AccountName: *usr,
			Digits:      otp.Digits(cf.OTP_DIGITS),
			Issuer:      cf.OTP_ISSUER,
			Period:      cf.OTP_TIMEOUT,
		}
		key, err := totp.Generate(gopts)
		assert(err)
		qrterminal.Generate(key.String(), qrterminal.L, os.Stdout)
		otpkey := key.Secret()
		assert(authDb.Set(*usr, otpkey))
		os.Exit(0)
	}
}
