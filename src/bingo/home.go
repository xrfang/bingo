package main

import (
	"net/http"
)

func home(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		if modified(w, r) {
			sendAsset(w, r.URL.Path)
		}
		return
	}
	user := guard.Validate(r)
	if user == "" {
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}
	renderTemplate(w, "explorer.html", struct{ User string }{user})
}
