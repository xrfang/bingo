package main

import (
	"net/http"
)

func favicon(w http.ResponseWriter, r *http.Request) {
	if !modified(w, r) {
		return
	}
	sendAsset(w, "imgs/logo.png")
}
