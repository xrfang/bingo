package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

func view(w http.ResponseWriter, r *http.Request) {
	path := r.URL.Query().Get("path")
	mime := r.URL.Query().Get("mime")
	if mime == "" {
		mime = "application/octet-stream"
	}
	f, err := os.Open(path)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer f.Close()
	st, _ := f.Stat()
	ims, err := time.Parse(time.RFC1123, r.Header.Get("If-Modified-Since"))
	if err == nil && !st.ModTime().After(ims) {
		http.Error(w, http.StatusText(http.StatusNotModified), http.StatusNotModified)
		return
	}
	disp := fmt.Sprintf("inline; filename=%s", filepath.Base(path))
	w.Header().Set("Content-Type", mime)
	w.Header().Set("Content-Disposition", disp)
	w.Header().Set("Content-Length", strconv.Itoa(int(st.Size())))
	w.Header().Set("Last-Modified", st.ModTime().Format(time.RFC1123))
	io.Copy(w, f)
}
