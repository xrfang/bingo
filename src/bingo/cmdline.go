package main

import (
	"net/http"
)

func cmdline(w http.ResponseWriter, r *http.Request) {
	user := guard.Validate(r)
	if user == "" {
		http.Redirect(w, r, "/login", http.StatusTemporaryRedirect)
		return
	}
	renderTemplate(w, "cmdline.html", struct{ User string }{user})
}
