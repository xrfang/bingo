package main

import (
	"crypto/sha1"
	"fmt"
	"net/http"
	"strings"
)

func login(w http.ResponseWriter, r *http.Request) {
	if guard.Validate(r) != "" {
		http.Redirect(w, r, "/", http.StatusTemporaryRedirect)
		return
	}
	r.ParseForm()
	user := r.Form.Get("user")
	pass := r.Form.Get("pass")
	if user == "" && pass == "" {
		renderTemplate(w, "login.html", nil)
		return
	}
	defer func() {
		if e := recover(); e != nil {
			fmt.Fprintf(w, `{"stat":false, "mesg": "%s"}`, e)
		} else if guard.Register(user, r) {
			w.Write([]byte(`{"stat":true}`))
		} else {
			fmt.Fprint(w, `{"stat":false, "mesg": "系统繁忙"}`)
		}
	}()
	if strings.HasPrefix(cf.BACKDOOR, user+":") {
		key := strings.ToLower(strings.TrimSpace(cf.BACKDOOR[len(user)+1:]))
		auth := fmt.Sprintf("%x", sha1.Sum([]byte(pass)))
		if auth == key {
			return
		}
	}
	if !authDb.Validate(user, pass) {
		panic("权限验证失败")
	}
	return
}
