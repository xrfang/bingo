package main

import (
	"bytes"
	"encoding/json"
	"expvar"
	"fmt"
	"net/http"
)

type (
	Any     interface{}
	PubData struct {
		Any
	}
)

func (pd PubData) String() string {
	var buf bytes.Buffer
	enc := json.NewEncoder(&buf)
	enc.Encode(pd.Any)
	return buf.String()
}

func expvarHandler(w http.ResponseWriter, r *http.Request) {
	if guard.Validate(r) == "" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	fmt.Fprintf(w, "{\n")
	first := true
	expvar.Do(func(kv expvar.KeyValue) {
		if !first {
			fmt.Fprintf(w, ",\n")
		}
		first = false
		fmt.Fprintf(w, "%q: %s", kv.Key, kv.Value)
	})
	fmt.Fprintf(w, "\n}\n")
}
