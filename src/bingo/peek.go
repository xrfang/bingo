package main

import (
	"fmt"
	"net/http"
)

func peek(w http.ResponseWriter, r *http.Request) {
	if guard.Validate(r) == "" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	f, ok := w.(http.Flusher)
	if !ok {
		http.Error(w, "flusher not supported", http.StatusInternalServerError)
		return
	}
	w.Write([]byte("connecting to bingo peek endpoint... "))
	f.Flush()
	ch := logger.Connect()
	w.Write([]byte(fmt.Sprintf("channel opened (%v)\n", ch)))
	f.Flush()
	for {
		msg, ok := <-ch
		if !ok {
			return
		}
		for _, m := range msg {
			w.Write([]byte(m + "\n"))
		}
		f.Flush()
	}
}
