package main

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
)

func upload(w http.ResponseWriter, r *http.Request) {
	if guard.Validate(r) == "" {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}
	defer func() {
		if e := recover(); e != nil {
			w.Write([]byte(fmt.Sprintf(`{"stat":false,"mesg":"%v"}`, e)))
		}
	}()
	fi, h, err := r.FormFile("file")
	assert(err)
	defer fi.Close()
	path := r.FormValue("path")
	if path == "" {
		panic(fmt.Errorf("path not specified"))
	}
	st, err := os.Stat(path)
	if err == nil && st.IsDir() {
		path = filepath.Join(path, h.Filename)
	}
	os.Rename(path, path+".backup_by_bingo")
	fo, err := os.Create(path)
	assert(err)
	defer fo.Close()
	n, err := io.Copy(fo, fi)
	assert(err)
	w.Write([]byte(fmt.Sprintf(`{"stat":true,"mesg":"%d bytes uploaded"}`, n)))
}
