package main

import (
	"encoding/json"
	"net/http"
)

func kill(w http.ResponseWriter, r *http.Request) {
	if guard.Validate(r) == "" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	ip := r.URL.Query().Get("ip")
	if ip == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	guard.Unregister(ip)
	enc := json.NewEncoder(w)
	guard.Lock()
	enc.Encode(guard.cli)
	guard.Unlock()
}
