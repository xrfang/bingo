package main

import (
	"os"
	"strings"
)

var ENV map[string]string

func init() {
	ENV = make(map[string]string)
	for _, e := range os.Environ() {
		kv := strings.SplitN(e, "=", 2)
		if len(kv) == 2 {
			ENV[kv[0]] = kv[1]
		}
	}
}
